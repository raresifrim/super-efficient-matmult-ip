# Super-Efficient IP for MATMULT
This is a mock-up of a Interactive Proof system where the Sum-Check protocol is applied on Matrix Multiplication. The implementation follows the defintions from Justin Thaler's book [ProofsArgsAndZK](https://people.cs.georgetown.edu/jthaler/ProofsArgsAndZK.pdf)

## Installation

### Dependency
The installation assumes that you have [rust](http://www.rust-lang.org) and it's package manager [cargo](https://doc.rust-lang.org/cargo/) installed on your working computer. If they aren't installed on your computer, you can follow the installation instructions on [rust website](https://www.rust-lang.org/tools/install). Cargo should be installed when you installed rust. Howerver, you can compiled it from source by following the instructions on [cargo documentation](https://doc.rust-lang.org/cargo/getting-started/installation.html).

Additional crates used:
- ark-ff
- ark-std
- rand 
- cell

## Usage

In order to run the prover-verifier interaction on the multipication of two randon NxN matrices simply run

```console
cargo run
```

Modulus and size of matrices are declared as global variables and can be changed. Currently it works with values than don't overflow over 64-bits.

```rust
//global types and consts
#[derive(MontConfig)]
#[modulus = "127"]
#[generator = "2"]
pub struct FqConfig;
pub type Fq = Fp64<MontBackend<FqConfig, 1>>;
const N:usize = 16; //matrix size
const LOG_N:usize = ((N*N) as u64).ilog2() as usize; //this is actually the size of the function input defined as an hyperboolean cube
```
