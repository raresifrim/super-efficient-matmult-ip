use ark_ff::fields::{Fp64, MontBackend, MontConfig};
use ark_ff::{BigInt, BigInteger,PrimeField};
use rand::prelude::*;
use once_cell::sync::Lazy;
use std::process;

//global types and consts
#[derive(MontConfig)]
#[modulus = "127"]
#[generator = "2"]
pub struct FqConfig;
pub type Fq = Fp64<MontBackend<FqConfig, 1>>;
const N:usize = 16; //matrix size
const LOG_N:usize = ((N*N) as u64).ilog2() as usize; //this is actually the size of the function input defined as an hyperboolean cube

fn sum_check(a:&[Fq; N*N], b:&[Fq; N*N], c:&[Fq; N*N]) {
    
    //generate transcript in advance and share it between prover and verifier
    static R3: Lazy<[u64;LOG_N/2]> = Lazy::new(||{
        let mut v = [0u64;LOG_N/2];
        for i in 0..LOG_N/2{
            let tmp = Fq::into_bigint(Fq::from(rand::random::<u64>())); //make sure we get a value inside the field 
            v[i] = tmp.0[0] as u64; //represent it as u64 for mle_eval function
        }
        v
    });


    let mut round_check = true;
    let mut g = Fq::from(0);
    for round in 0..(LOG_N/2){
        let (claim, proof) = prover(a, b, c, round, &*R3);
        println!("{claim} {:?}", proof);
        if round == 0 {
            (round_check, g) = verifier(claim, proof, round, &*R3);
        } else {
            (round_check, g) = verifier(g, proof, round, &*R3);
        }
        println!("{g}");
        if round_check == false {
            println!("Found inconsistency in interactive proof protocol! Exiting...");
            process::exit(1);
        }
    }

    //perform final oracle check
    //we use prover as oracle to simplify things
    let (oracle_query, _) = prover(a, b, c, LOG_N/2, &*R3);
    println!("{oracle_query}");
    //last verifier polynomial g should be equal to the oracle query
    if oracle_query != g {
        println!("Found inconsistency in interactive proof protocol! Exiting...");
        process::exit(1);
    }
    println!("Interactive proof protocol finished completely and successfully!");
}

fn verifier(claim:Fq, proof:[Fq;3], round:usize, transcript:&[u64]) -> (bool,Fq) {

    let g0 = lagrange_interpolation(proof, Fq::from(0));
    let g1 = lagrange_interpolation(proof, Fq::from(1));
    if g0+g1 != claim {
        (false, Fq::from(0))
    } else {
        //prepare evaluation of old g polynomial on "current" random value
        (true, lagrange_interpolation(proof, transcript[round].into()))
    } 
}

//function returns a claim plus a commitment to a polynomial
fn prover(a:&[Fq; N*N], b:&[Fq; N*N], c:&[Fq; N*N], round:usize, r3:&[u64]) -> (Fq,[Fq;3]) {

    //we prepare random r1 and r2 in prover
    static R1: Lazy<[u64;LOG_N/2]> = Lazy::new(|| { 
        let mut v = [0u64;LOG_N/2];
        for i in 0..LOG_N/2{
            let tmp = Fq::into_bigint(Fq::from(rand::random::<u64>())); //make sure we get a value inside the field 
            v[i] = tmp.0[0] as u64; //represent it as u64 for mle_eval function
        }
        v
    });
    static R2: Lazy<[u64;LOG_N/2]> = Lazy::new(||{
        let mut v = [0u64;LOG_N/2];
        for i in 0..LOG_N/2{
            let tmp = Fq::into_bigint(Fq::from(rand::random::<u64>())); //make sure we get a value inside the field 
            v[i] = tmp.0[0] as u64; //represent it as u64 for mle_eval function
        }
        v
    });


    //define uni-variate polynomial g
    //being a quadratic poly we only need to send evaluations in {0,1,2}
    let mut g = [Fq::from(0); 3];  
    let mut ra = [0u64;LOG_N];
    let mut rb = [0u64;LOG_N];

    //fill half of vectors A and B with r1,r2 which remain fixed
    for k in 0..LOG_N/2 {
        ra[LOG_N/2 + k] = R1[k];
        rb[k] = R2[k];
    }

    //fill A and B with random values collected from verifier from previous rounds
    //in round 0 this will be skipped
    for i in 0..round{
        ra[i] = r3[i];
        rb[LOG_N/2 + i] = r3[i];
    }
    
    if round < LOG_N/2 {
        for x in 0..=2{
            //then set x={0,1,2} on free variable
            ra[round] = x as u64;
            rb[LOG_N/2 + round] = x as u64;

            //finally avaluate g(x) on remaining boolean values
            let boolean_combinaions = u64::pow(2, (LOG_N/2 - 1 - round).try_into().unwrap());
            for k in 0..boolean_combinaions {
                for index in round..(LOG_N/2-1) {
                    ra[index + 1] = ((k >> (index-round)) & 0x1) as u64;
                    rb[LOG_N/2 + index + 1] = ((k >> (index-round)) & 0x1) as u64;
                }
                println!("RA = {:?}",ra);
                println!("RB = {:?}",rb);
                g[x] += mle_eval(a, &ra) * mle_eval(b, &rb);
            }
        }
    
        //in round 0 we send MLE of C
        if round == 0 {
            let mut r = [0u64; LOG_N];
            for i in 0..LOG_N/2{
                r[i + LOG_N/2] = R1[i];
                r[i] = R2[i];
            }
            //get initial claim which is equal to mle eval of C matrix in r1,r2
            let mle_fc = mle_eval(c, &r);
            assert_eq!(mle_fc,g[0]+g[1]);
            return (mle_fc, g);
        }
        return (g[0] + g[1], g);
    
    } else {
        println!("RA = {:?}",ra);
        println!("RB = {:?}",rb);
        g[0] = mle_eval(a, &ra) * mle_eval(b, &rb);
        return (g[0], g);
    }

}

fn lagrange_basis(x:Fq, i:usize) -> Fq {
    //this computes the lagrange basis polynomial for an univariate quadratic polynomial
    //for this particular case, it is enough to send three point evaluations in order to evaluate
    //any random point
    let mut basis = Fq::from(1);
    for k in 0..3 {
        if k != i {
            basis *= (x - Fq::from(k as u64))/Fq::from((i as i64) - (k as i64)); 
        }
    }

    return basis;
}

fn lagrange_interpolation(f:[Fq; 3], x:Fq) -> Fq {
    //this evaluates a polynomial at a given input x, based on the point evaluation array f 
    //as we know that three points are enough to evaluate a univariate quadratic polynomial through
    //Lagrange interpolation, we limit the main loop to three iterations
    let mut poly_result = Fq::from(0);
    for k in 0..3 {
        poly_result += f[k] * lagrange_basis(x, k);
    }
    return poly_result;
}

fn mle_basis(x:&[u64], w:u64) -> Fq {
    let mut basis:i64 = 1;
    for k in 0..LOG_N {
        //we should not need more than 64 bit numbers
        //we need the signed version for correct evaluation
        let x_bit:i64 = x[k] as i64;
        let w_bit:i64 = ((w >> k) & 0x1).try_into().unwrap();
        basis *= x_bit * w_bit + (1 - x_bit)*(1 - w_bit);
    }
    return Fq::from(basis);
}

fn mle_eval(m:&[Fq; N*N], x:&[u64]) -> Fq {
    let mut mle_result = Fq::from(0);
    for k in 0..N*N {
        mle_result += m[k] * mle_basis(x, k as u64);
    }
    return mle_result;
}

fn matmult(a:&[Fq; N*N], b:&[Fq; N*N]) -> [Fq; N*N] {
    
    let mut c = [Fq::from(0); N*N]; 
    for i in 0..N {
        for j in 0..N {
            for k in 0..N {
                c[i*N + j] += a[i*N + k] * b[k*N + j];
            }
        } 
    }
 
    return c;
}


fn test_mle_equality(a:&[Fq; N*N], b:&[Fq; N*N], c:&[Fq; N*N], x:u64, y:u64) {
    //test over extended  domain
    let index = x*(N as u64) + y;
    let mut rc = [0u64; LOG_N];
    if index < (N*N).try_into().unwrap() { //write index as hyperboolean cube
        for k in 0..LOG_N {
            rc[k] = ((index >> k) & 0x1) as u64; 
        } 
    } else { //othersise try to map x,y in r vector
        rc[LOG_N/2 - 1] = x; 
        rc[LOG_N - 1] = y;
    } 
    let mle_c = mle_eval(c, &rc);
    //println!("MLE(C) -> {mle_c}");
    //println!("RC -> {:?}",rc);

    //evaluate mle of a and b in common coordintates over all boolean inputs in other coordinate
    let mut ra = [0u64; LOG_N];
    let mut rb = [0u64; LOG_N];
    let mut equality = Fq::from(0);
    
    if index < (N*N).try_into().unwrap() { //write index as hyperboolean cube
        for k in 0..LOG_N/2 {
            ra[LOG_N/2 + k] = ((index >> (LOG_N/2 + k)) & 0x1) as u64;
            rb[k] = ((index >> k) & 0x1) as u64;
        } 
    } else { //othersise try to map x,y in r vector
        for k in 0..LOG_N/2 {
            ra[LOG_N/2 + k] = rc[LOG_N/2 + k];
            rb[k] = rc[k];
        }
    }

    let boolean_combinaions = u64::pow(2, (LOG_N/2).try_into().unwrap());
    for k in 0..boolean_combinaions {
        for index in 0..LOG_N/2 {
            ra[index] = ((k >> index) & 0x1) as u64;
            rb[LOG_N/2 + index] = ((k >> index) & 0x1) as u64;
        }
        //println!("RA -> {:?}", ra);
        //println!("RB -> {:?}", rb);
        let mle_a = mle_eval(a, &ra); 
        let mle_b = mle_eval(b, &rb);
        //println!("MLE(A) -> {mle_a}");
        //println!("MLE(B) -> {mle_b}");
        equality += mle_a * mle_b;
    }
    
    //check if equality holds
    assert_eq!(mle_c, equality);
}

fn main() {
    
    let mut a = [Fq::from(0); N*N];
    let mut b = [Fq::from(0); N*N];

    //just fill the matrixes with some values
    for i in 0..N*N {
        a[i] = Fq::from(rand::random::<u64>());
        b[i] = Fq::from(rand::random::<u64>());
    }

    //get real value of c=axb as computed using classic approach
    let c = matmult(&a, &b);

    //check if C = AxB also holds in multi-linear extension over random inputs
    test_mle_equality(&a,&b,&c,1,2);
    test_mle_equality(&a,&b,&c,3,3);
    test_mle_equality(&a,&b,&c,25,56);
    test_mle_equality(&a,&b,&c,144,232);

    //run sum-check interactive proof protocol
    sum_check(&a, &b, &c);
}
